if (exists("g:notes_loaded"))
  finish
endif

" Options
if (exists('g:notes_directory') == 0)
  let g:notes_directory = "~/.notes"
endif
if (exists('g:notes_auto_rename') == 0)
  let g:notes_auto_rename = 1
endif

" Rename a note file based on the first line that appears in it.
function! NotesGetNewFileName()
  let newFileName = readfile(expand('%'))[0]
  if (len(newFileName) == 0)
    return ''
  endif

  " Remove special/illegal characters
  let newFileName = substitute(newFileName, "[#'\\\"/:!]", '', 'g')
  " Get rid of extra whitespace
  let newFileName = trim(newFileName)
  let newFileName = substitute(newFileName, ' ', '_', 'g')
  " Add file extension
  return printf('%s.md', newFileName)

endfunction

" Open a file in a split and apply buffer-specific auto-commands and keybinds.
function! NotesOpen(path)
  :execute "e " . a:path
  autocmd! BufWritePost <buffer> call NotesRename()
endfunction

" Determine whether a given buffer should be auto-renamed on save.
function! NotesCreateAutoRenameCommand()
  let curFilename = expand('%:h') . '/'
  let fullNotesDir = expand(g:notes_directory)
  let isInNotesDir = match(curFilename, fullNotesDir) > -1
  if isInNotesDir && g:notes_auto_rename == 1
    autocmd! BufWritePost <buffer> call NotesRename()
  endif
endfunction

autocmd! BufEnter *.md call NotesCreateAutoRenameCommand()

" Rename a note based on the content of the first line.
function! NotesRename()
  if (g:notes_auto_rename == 0)
    return
  endif

  let l:newName = NotesGetNewFileName()

  " Heavily based on the work from these fine folks: https://github.com/danro/rename.vim/blob/master/plugin/rename.vim
  let l:curFile = expand("%:p")
  let l:curPath = expand("%:h")
  try
    silent! exe "saveas " . fnameescape(l:curPath . '/' . l:newName)
    let l:prevFile = l:curFile
    let l:curFile = expand("%:p")
    if l:curFile !=# l:prevFile && filewritable(l:curFile)
      silent exe "bwipe! " . fnameescape(l:prevFile)
      if delete(l:prevFile)
        echoerr "Could not delete " . l:prevFile
      endif
    endif
  catch
    echoerr "Ran into trouble renaming file: " . l:curFile
  endtry
endfunction

function! NotesOpenQuickFix()
  copen
  autocmd! BufLeave <buffer> :q
  nnoremap <buffer> q :q<CR>
endfunction

" Create a new note.
function! NotesCreate()
  let l:tmp_file_name = strftime('%y%m%d%H%M%S') . '.md'
  call NotesOpen(g:notes_directory . l:tmp_file_name)
endfunction

command! -nargs=* -bang NotesCreate call NotesCreate()

" Show a list of all notes in the QuickFix list.
function! NotesList()
  let search_results = ""
  if (executable('rg') == 1)
    let search_results = systemlist('rg -irn --files-with-matches --sortr modified "" ' . g:notes_directory)
  else
    let search_results = systemlist('grep -irn --files-with-match "" ' . g:notes_directory)
  endif
  let qf_items = getqflist({
        \ 'efm': '^%f$', 
        \ 'lines': search_results
        \ })

  " Store the filename so we can navigate to the item later.
  for i in qf_items.items
    let i.filename = i.text
  endfor

  " Format the line text so it looks nice.
  function! GetLineText(info)
    let l:lines = []
    let items = getqflist({'id' : a:info.id, 'items' : 1}).items
    for i in items
      let file_name = split(i.text, '/')[-1]
      call add(l:lines, file_name)
    endfor
    return l:lines
  endfunction

  call setqflist([], 'r', {
        \ 'items': qf_items.items,
        \ 'title': 'All Notes',
        \ 'quickfixtextfunc': 'GetLineText'
        \ })
  call NotesOpenQuickFix()
endfunction

command! -nargs=* -bang NotesList call NotesList()

" Locate a note using Grep + the QuickFix list.
function! NotesSearch()
  let query = input('Find Note Containing: ')
  if (len(query) > 0)
    let search_results = systemlist('grep -irn "' . query . '" ' . g:notes_directory)
    let qf_items = getqflist({
          \ 'efm': '%f:%l:%m', 
          \ 'lines': search_results
          \ })
    call setqflist([], 'r', {
          \ 'items': qf_items.items,
          \ 'title': 'Notes Containing "' . query . '"'
          \ })
    call NotesOpenQuickFix()
  endif
endfunction

command! -nargs=* -bang NotesSearch call NotesSearch()

" Locate a note using the FZF fuzzy-finder.
" Requires: RipGrep and FZF
function! NotesSearchFuzzy(query)
  if (executable('rg') == 0 || exists('g:loaded_fzf_vim') == 0)
    throw "Requires RipGrep and FZF.vim"
  endif

  let command_fmt = 'rg 
        \ --files-with-matches
        \ --sortr modified
        \ --column
        \ --line-number
        \ --no-heading
        \ --color=always 
        \ --smart-case 
        \ -- %s %s || true'
  let initial_command = printf(command_fmt, shellescape(a:query), g:notes_directory)
  let reload_command = printf(command_fmt, '{q}', g:notes_directory)
  let spec = {'options': [
        \ '--cycle',
        \ '--prompt', 'Search: ',
        \ '--ansi',
        \ '--phony', 
        \ '--delimiter', '/',
        \ '--with-nth', '-1',
        \ '--query', a:query, 
        \ '--preview', 'rg -i --no-heading {q} {}',
        \ '--bind', 'change:reload:'.reload_command],
        \ 'source': initial_command,
        \ 'sink': function('NotesOpen')}
  call fzf#run(spec)
endfunction

command! -nargs=* -bang NotesSearchFuzzy call NotesSearchFuzzy(<q-args>)

" Open the notes directory in Netrw to move, rename, delete, etc. items.
function! NotesManage()
  :execute "Vexplore " . g:notes_directory
endfunction

command! -nargs=* -bang NotesManage call NotesManage()
