# 🗒️ Notes.vim

A very simple note-taking aid for (Neo)Vim with minimal dependencies.

## Commands

| Command       | Description                                                                                                                               |
| -             | -                                                                                                                                         |
| `NotesCreate` | Create a new note and save it to a pre-configured directory (see `g:notes_directory`).                                                    |
| `NotesList`   | Lists all notes in the QuickFix buffer.  If you have RipGrep installed, they will be listed by modified date in descending order (i.e. newest at the top).       |
| `NotesSearch` | Populate the QuickFix list with a list of notes that contain the search string.                                                           |
| `NotesSearchFuzzy(query)` | Search notes using RipGrep and FZF (if available).  |
| `NotesManage`             | Open the notes directory in `netrw` so they can be renamed, deleted, etc. |   |

## Options

| Options               | Description                           | Default    |
| -                     | -                                     | -          |
| `g:notes_directory`   | Directory to save notes to.           | `~/.notes` |
| `g:notes_auto_rename` | Use first line to determine file name | 1          |

## Dependencies

Notes.vim should work well on any Unix-ish system that has `grep` and the like.  Optional dependencies are:
| Dependency | Supports                 |
| -          | -                        |
| `ripgrep`  | Fuzzy-finding            |
| `fzf.vim`  | Fuzzy-finding            |

## Installation
Add something like this to your `init.lua` file:

```lua
-- Alias Plug command to make things look nicer.
local Plug = vim.fn["plug#"]

-- Install plugins
vim.call("plug#begin", "~/.config/nvim/plugged")

-- Note-taking
Plug "https://gitlab.com/atomdmac/notes.vim"

-- Dependencies
Plug("junegunn/fzf", {["dir"] = "~/.fzf", ["do"] = "./install --all"})
Plug "junegunn/fzf.vim"

vim.call("plug#end")
```

## Keybindings
This plugin provides no default Keybindings out of the box but the ones I use are:
```vimscript
nmap <space>nn :NotesCreate<CR>
nmap <space>nl :NotesList<CR>
nmap <space>nf :NotesSearchFuzzy<CR>
nmap <space>nF :NotesSearch<CR>
nmap <space>nm :NotesManange<CR>
```

## Multi-Device Sync

Since Notes.vim just saves text to a regular directory, you can use any general-purpose file-sync service like [Dropbox](https://www.dropbox.com/) or [Seafile](https://www.seafile.com/en/home/) to back up and sync your notes between devices.

## Plug-in Pairings
I've found that `notes.vim` works really well with the following plug-ins:
| Plug-in                                                                 | Description                                       |
| -                                                                       | -                                                 |
| [markdown-preview.nvim](https://github.com/iamcco/markdown-preview.vim) | Live preview of formatted markdown in a browser   |
| [vim-markdown](https://github.com/plasticboy/vim-markdown)              | Markdown formatting and tools                     |
| [vim-markdown-folding](https://github.com/masukomi/vim-markdown-folding) | Additional folding functionality |
 [vimwiki](https://github.com/vimwiki/vimwiki) | Great for creating links between documents |
 [scratch.vim](https://github.com/mtth/scratch.vim) | Ephemeral buffer for taking quick notes |

Notes.vim was designed to be as small and dependency-free as possible so your other favorite note-taking plug-ins should play nicely with it, too.  If they don't, please open an issue.

## Prior Art

* [vim-pad](https://github.com/fmoralesc/vim-pad)
